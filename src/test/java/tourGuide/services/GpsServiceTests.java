package tourGuide.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import tourguide.models.User;
import tourguide.services.GpsService;

@ExtendWith(MockitoExtension.class)
public class GpsServiceTests {

    @Mock
    GpsUtil gpsUtil;

    @InjectMocks
    GpsService gpsService;

    @Test
    public void getUserLocationTest() {
        User user = new User(new UUID(0, 0), "username", "phone", "mail");
        gpsService.getUserLocation(user);
        verify(gpsUtil).getUserLocation(user.getUserId());
    }

    @Test
    public void getUserLocationNull() {
        User user = null;
        assertThrows(NullPointerException.class, () -> {
            gpsService.getUserLocation(user);
        });
    }

    @Test
    public void getAttractionsTest() {
        gpsService.getAttractions();
        verify(gpsUtil).getAttractions();
    }

    @Test
    public void getDistanceValue1() {
        Location loc1 = new Location(26.890959, -80.116577);
        Location loc2 = new Location(-26.89957, -73.741795);
        Double distance = gpsService.getDistance(loc2, loc1);
        assertEquals(3738.1212319687047, distance);
    }

    @Test
    public void getDistanceValue2() {
        Location loc1 = new Location(0.0, 10.0);
        Location loc2 = new Location(0.0, 10.0);
        Double distance = gpsService.getDistance(loc2, loc1);
        assertEquals(0.0, distance);
    }

    @Test
    public void getAttractionsByDistanceTest() {
        Location referenceLocation = new Location(0, 0);

        List<Attraction> list = new ArrayList<>();
        Attraction attraction1 = new Attraction("attraction1", "city", "state", 50.0, 50.0);
        Attraction attraction2 = new Attraction("attraction2", "city", "state", 10.0, 10.0);
        Attraction attraction3 = new Attraction("attraction3", "city", "state", 80.0, 80.0);
        Attraction attraction4 = new Attraction("attraction4", "city", "state", 15.0, 15.0);

        list.add(attraction1);
        list.add(attraction2);
        list.add(attraction3);
        list.add(attraction4);

        when(gpsUtil.getAttractions()).thenReturn(list);

        List<Attraction> result = gpsService.getAttractionsByDistance(referenceLocation);

        assertEquals("attraction2", result.get(0).attractionName);
        assertEquals("attraction4", result.get(1).attractionName);
        assertEquals("attraction1", result.get(2).attractionName);
        assertEquals("attraction3", result.get(3).attractionName);

    }
}
