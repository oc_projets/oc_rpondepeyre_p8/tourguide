package tourGuide.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.services.TripDealService;
import tripPricer.Provider;
import tripPricer.TripPricer;

@ExtendWith(MockitoExtension.class)
public class TripDealServiceTests {

    @Mock
    TripPricer tripPricer;

    @InjectMocks
    TripDealService tripDealService;

    @Test
    public void getTripDealsTests() {
        User user = new User(new UUID(0, 0), "username", "phone", "mail");

        UserPreferences userPreferences = new UserPreferences();

        CurrencyUnit currency = Monetary.getCurrency("USD");
        userPreferences.setHighPricePoint(Money.of(80.0, currency));
        userPreferences.setLowerPricePoint(Money.of(30.0, currency));
        user.setUserPreferences(userPreferences);
        List<Provider> providers = new ArrayList<>();

        Provider provider1 = new Provider(new UUID(1, 1), "provider 1", 90.0);
        Provider provider2 = new Provider(new UUID(2, 2), "provider 2", 20.0);
        Provider provider3 = new Provider(new UUID(3, 3), "provider 3", 50.0);
        providers.add(provider1);
        providers.add(provider2);
        providers.add(provider3);
        when(tripPricer.getPrice(anyString(), any(), anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(providers);

        List<Provider> result = tripDealService.getTripDeals(user);

        assertEquals(1, result.size());
        assertEquals("provider 3", result.get(0).name);
    }

}
