package tourGuide.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourguide.dto.NearbyAttractionDto;
import tourguide.dto.UserPreferencesDto;
import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.repositories.UserRepository;
import tourguide.services.GpsService;
import tourguide.services.RewardsService;
import tourguide.services.TripDealService;
import tourguide.services.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository repository;
    @Mock
    GpsService gpsService;
    @Mock
    RewardsService rewardsService;
    @Mock
    TripDealService tripDealService;

    @InjectMocks
    UserService userService;

    @Test
    public void getUserTest() {
        User user = new User(new UUID(0, 0), "username", "phone", "mail");
        when(repository.findByUsername("username")).thenReturn(Optional.of(user));
        userService.getUser("username");
        verify(repository).findByUsername("username");
    }

    @Test
    public void getUserNotFound() {
        assertThrows(IllegalArgumentException.class, () -> userService.getUser("username"));
    }

    @Test
    public void getUserLocation() {
        User user = new User(new UUID(0, 0), "username", "phone", "mail");
        user.addToVisitedLocations(
                new VisitedLocation(new UUID(0, 0), new Location(0, 0), Date.valueOf(LocalDate.now())));

        userService.getUserLocation(user);

        verify(gpsService, Mockito.times(0)).getUserLocation(user);
    }

    @Test
    public void getUserLocationWithGPS() {
        User user = new User(new UUID(0, 0), "username", "phone", "mail");
        userService.getUserLocation(user);

        verify(gpsService, Mockito.times(1)).getUserLocation(user);
    }

    @Test
    public void getAllCurrentLocationsTest() {

        List<User> users = new ArrayList<>();
        User user1 = new User(UUID.randomUUID(), "user1", "phone", "mail");
        User user2 = new User(UUID.randomUUID(), "user2", "phone", "mail");
        VisitedLocation location = new VisitedLocation(UUID.randomUUID(), new Location(0, 0),
                Date.valueOf(LocalDate.now()));

        user1.addToVisitedLocations(location);
        user2.addToVisitedLocations(location);
        users.add(user1);
        users.add(user2);
        when(repository.findallUsers()).thenReturn(users);
        Map<UUID, Location> result = userService.getAllCurrentLocations();

        assertEquals(2, result.size());

    }

    @Test
    public void getNearByAttractionsTest() {
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");
        VisitedLocation visitedLocation = new VisitedLocation(UUID.randomUUID(), new Location(0, 0),
                Date.valueOf(LocalDate.now()));

        when(gpsService.getUserLocation(user)).thenReturn(visitedLocation);
        Attraction attraction1 = new Attraction("attraction1", "city", "state", 0, 0);
        Attraction attraction2 = new Attraction("attraction2", "city", "state", 0, 0);
        Attraction attraction3 = new Attraction("attraction3", "city", "state", 0, 0);
        Attraction attraction4 = new Attraction("attraction4", "city", "state", 0, 0);
        Attraction attraction5 = new Attraction("attraction5", "city", "state", 0, 0);

        List<Attraction> attractions = new ArrayList<>();

        attractions.add(attraction1);
        attractions.add(attraction2);
        attractions.add(attraction3);
        attractions.add(attraction4);
        attractions.add(attraction5);

        when(gpsService.getAttractionsByDistance(visitedLocation.location)).thenReturn(attractions);

        List<NearbyAttractionDto> result = userService.getNearByAttractions(user, 3);

        assertEquals(3, result.size());
        assertEquals(NearbyAttractionDto.class, result.get(0).getClass());
    }

    @Test
    public void updateUserPreferencesTestEmpty() {
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");

        UserPreferences preferences = new UserPreferences();
        preferences.setAttractionProximity(0);
        preferences.setCurrency(Monetary.getCurrency("USD"));
        preferences.setHighPricePoint(Money.of(10D, Monetary.getCurrency("USD")));
        preferences.setLowerPricePoint(Money.of(1D, Monetary.getCurrency("USD")));
        preferences.setNumberOfAdults(2);
        preferences.setNumberOfChildren(1);
        preferences.setTicketQuantity(3);
        preferences.setTripDuration(20);

        user.setUserPreferences(preferences);

        when(repository.findByUsername("user1")).thenReturn(Optional.of(user));
        when(repository.updateUser(any(User.class))).thenReturn(Optional.of(user));

        UserPreferencesDto dto = new UserPreferencesDto();
        dto.setUserName("user1");

        userService.updateUserPreferences(dto);

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

        verify(repository).updateUser(captor.capture());
        UserPreferences result = captor.getValue().getUserPreferences();

        assertEquals(0, result.getAttractionProximity());
        assertEquals(Monetary.getCurrency("USD"), result.getCurrency());
        assertEquals(Money.of(10D, Monetary.getCurrency("USD")), result.getHighPricePoint());
        assertEquals(Money.of(1D, Monetary.getCurrency("USD")), result.getLowerPricePoint());
        assertEquals(2, result.getNumberOfAdults());
        assertEquals(1, result.getNumberOfChildren());
        assertEquals(3, result.getTicketQuantity());
        assertEquals(20, result.getTripDuration());
    }

    @Test
    public void updateUserPreferencesTest() {
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");

        UserPreferences preferences = new UserPreferences();
        preferences.setAttractionProximity(2);
        preferences.setCurrency(Monetary.getCurrency("EUR"));
        preferences.setHighPricePoint(Money.of(0D, Monetary.getCurrency("USD")));
        preferences.setLowerPricePoint(Money.of(0D, Monetary.getCurrency("USD")));
        preferences.setNumberOfAdults(1);
        preferences.setNumberOfChildren(0);
        preferences.setTicketQuantity(1);
        preferences.setTripDuration(10);

        user.setUserPreferences(preferences);

        when(repository.findByUsername("user1")).thenReturn(Optional.of(user));
        when(repository.updateUser(any(User.class))).thenReturn(Optional.of(user));

        UserPreferencesDto dto = new UserPreferencesDto();
        dto.setUserName("user1");
        dto.setAttractionProximity(0);
        dto.setCurrency(Monetary.getCurrency("USD"));
        dto.setHighPricePoint(10D);
        dto.setLowerPricePoint(1D);
        dto.setNumberOfAdults(2);
        dto.setNumberOfChildren(1);
        dto.setTicketQuantity(3);
        dto.setTripDuration(20);

        userService.updateUserPreferences(dto);

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

        verify(repository).updateUser(captor.capture());
        UserPreferences result = captor.getValue().getUserPreferences();

        assertEquals(0, result.getAttractionProximity());
        assertEquals(Monetary.getCurrency("USD"), result.getCurrency());
        assertEquals(Money.of(10D, Monetary.getCurrency("USD")), result.getHighPricePoint());
        assertEquals(Money.of(1D, Monetary.getCurrency("USD")), result.getLowerPricePoint());
        assertEquals(2, result.getNumberOfAdults());
        assertEquals(1, result.getNumberOfChildren());
        assertEquals(3, result.getTicketQuantity());
        assertEquals(20, result.getTripDuration());
    }

    @Test
    public void UpdateUserErrorTest() {
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");
        when(repository.findByUsername("user1")).thenReturn(Optional.of(user));
        when(repository.updateUser(any(User.class))).thenReturn(Optional.empty());
        UserPreferencesDto dto = new UserPreferencesDto();
        dto.setUserName("user1");

        assertThrows(IllegalArgumentException.class, () -> userService.updateUserPreferences(dto), "User not found");
    }

    @Test
    public void getTripDealsTest() {
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");
        userService.getTripDeals(user);
        verify(tripDealService).getTripDeals(user);
    }
}
