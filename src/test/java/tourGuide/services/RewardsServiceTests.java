package tourGuide.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourguide.models.User;
import tourguide.models.UserReward;
import tourguide.services.GpsService;
import tourguide.services.RewardsService;

@ExtendWith(MockitoExtension.class)
public class RewardsServiceTests {

    @Mock
    GpsService gpsService;
    @Mock
    RewardCentral rewardsCentral;

    @InjectMocks
    RewardsService rewardsService;

    @Test
    public void calculateRewardsTests() {

        VisitedLocation visitedLocation = new VisitedLocation(new UUID(0, 0), new Location(0, 0),
                Date.valueOf(LocalDate.now()));
        Attraction attraction = new Attraction("attraction", "city", "state", 0, 0);
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");
        user.addToVisitedLocations(visitedLocation);

        List<Attraction> attractions = new ArrayList<>();
        attractions.add(attraction);

        when(gpsService.getAttractions()).thenReturn(attractions);

        rewardsService.calculateRewards(user);

        assertEquals(1, user.getUserRewards().size());

    }

    @Test
    public void calculateRewardsTests_alreadystored() {

        VisitedLocation visitedLocation = new VisitedLocation(new UUID(0, 0), new Location(0, 0),
                Date.valueOf(LocalDate.now()));
        Attraction attraction = new Attraction("attraction", "city", "state", 0, 0);
        User user = new User(UUID.randomUUID(), "user1", "phone", "mail");
        user.addToVisitedLocations(visitedLocation);
        user.addUserReward(new UserReward(visitedLocation, attraction));

        List<Attraction> attractions = new ArrayList<>();
        attractions.add(attraction);

        when(gpsService.getAttractions()).thenReturn(attractions);

        rewardsService.calculateRewards(user);

        assertEquals(1, user.getUserRewards().size());

    }
}
