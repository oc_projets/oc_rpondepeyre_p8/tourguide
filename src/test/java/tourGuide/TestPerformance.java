package tourGuide;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import tourguide.Application;
import tourguide.models.User;
import tourguide.services.GpsService;
import tourguide.services.RewardsService;
import tourguide.services.UserService;

@SpringBootTest(classes = Application.class)
public class TestPerformance {

	/*
	 * A note on performance improvements:
	 * 
	 * The number of users generated for the high volume tests can be easily
	 * adjusted via this method:
	 * 
	 * InternalTestHelper.setInternalUserNumber(100000);
	 * 
	 * 
	 * These tests can be modified to suit new solutions, just as long as the
	 * performance metrics
	 * at the end of the tests remains consistent.
	 * 
	 * These are performance metrics that we are trying to hit:
	 * 
	 * highVolumeTrackLocation: 100,000 users within 15 minutes:
	 * assertTrue(TimeUnit.MINUTES.toSeconds(15) >=
	 * TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 *
	 * highVolumeGetRewards: 100,000 users within 20 minutes:
	 * assertTrue(TimeUnit.MINUTES.toSeconds(20) >=
	 * TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */
	/*
	 * private final GpsService gpsService;
	 * private final RewardsService rewardsService;
	 * private final UserService userService;
	 * private final TrackerService trackerService;
	 * 
	 * public TestPerformance(GpsService gpsService, RewardsService rewardsService,
	 * UserService userService,
	 * TrackerService trackerService) {
	 * this.gpsService = gpsService;
	 * this.rewardsService = rewardsService;
	 * this.userService = userService;
	 * this.trackerService = trackerService;
	 * }
	 */

	@Autowired
	GpsService gpsService;

	@Autowired
	RewardsService rewardsService;

	@Autowired
	UserService userService;

	@Autowired
	Executor executor;

	@Test
	public void highVolumeTrackLocation() throws InterruptedException, ExecutionException {

		// Users should be incremented up to 100,000, and test finishes within 15
		// minutes

		List<User> allUsers = new ArrayList<>();
		allUsers = userService.getAllUsers();

		assertEquals(3, allUsers.get(10).getVisitedLocations().size());
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		userService.trackUsersLocations(allUsers);

		// System.out.println(test);
		stopWatch.stop();
		// trackerService.stopTracking();
		assertEquals(4, allUsers.get(10).getVisitedLocations().size());

		System.out.println("highVolumeTrackLocation: Time Elapsed: "
				+ TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Test
	public void highVolumeGetRewards() {

		// Users should be incremented up to 100,000, and test finishes within 20
		// minutes

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		Attraction attraction = gpsService.getAttractions().get(0);
		List<User> allUsers = new ArrayList<>();
		allUsers = userService.getAllUsers();
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

		rewardsService.calculateAllUsersRewards(allUsers);

		for (User user : allUsers) {
			assertTrue(user.getUserRewards().size() > 0);
		}
		stopWatch.stop();
		// trackerService.stopTracking(); ,

		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())
				+ " seconds.");
		assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

}
