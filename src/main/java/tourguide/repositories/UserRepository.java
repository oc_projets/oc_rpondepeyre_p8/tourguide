package tourguide.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import tourguide.models.User;

@Repository
public class UserRepository {

    private static List<User> users = new ArrayList<>();

    public List<User> findallUsers() {
        return users;
    }

    public Optional<User> findByUsername(String username) {
        for (User user : users) {
            if (user.getUserName().equals(username)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public void addUser(User user) {
        users.add(user);
    }

    public Optional<User> updateUser(User usertoUpdate) {
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            if (user.getUserName().equals(usertoUpdate.getUserName())) {
                user = usertoUpdate;
                users.set(i, user);
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }
}
