package tourguide.controllers;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourguide.dto.NearbyAttractionDto;
import tourguide.dto.UserPreferencesDto;
import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.models.UserReward;
import tourguide.services.TripDealService;
import tourguide.services.UserService;
import tripPricer.Provider;

@RestController
public class TourGuideController {

    private final UserService userService;

    public TourGuideController(TripDealService tripDealService, UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @GetMapping("/getLocation")
    public ResponseEntity<Location> getLocation(@RequestParam String userName)
            throws InterruptedException, ExecutionException {
        VisitedLocation visitedLocation = userService.getUserLocation(userService.getUser(userName));
        return new ResponseEntity<>(visitedLocation.location, HttpStatus.OK);
    }

    @GetMapping("/getNearbyAttractions")
    public ResponseEntity<List<NearbyAttractionDto>> getNearbyAttractions(@RequestParam String userName) {
        User user = userService.getUser(userName);
        return new ResponseEntity<>(userService.getNearByAttractions(user, 5), HttpStatus.OK);
    }

    @GetMapping("/getRewards")
    public ResponseEntity<List<UserReward>> getRewards(@RequestParam String userName) {
        return new ResponseEntity<>(userService.getUser(userName).getUserRewards(), HttpStatus.OK);
    }

    @GetMapping("/getAllCurrentLocations")
    public ResponseEntity<Map<UUID, Location>> getAllCurrentLocations() {
        return new ResponseEntity<>(userService.getAllCurrentLocations(), HttpStatus.OK);
    }

    @GetMapping("/getTripDeals")
    public ResponseEntity<List<Provider>> getTripDeals(@RequestParam String userName) {
        List<Provider> providers = userService.getTripDeals(userService.getUser(userName));
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }

    @PutMapping("/updatePreferences")
    public ResponseEntity<UserPreferences> updateUserPreferences(@RequestBody UserPreferencesDto preferences) {
        return new ResponseEntity<>(userService.updateUserPreferences(preferences), HttpStatus.OK);
    }
}