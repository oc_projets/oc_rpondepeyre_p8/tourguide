package tourguide.services;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourguide.models.User;

@Service
public class GpsService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    private final GpsUtil gpsUtil;

    public GpsService(GpsUtil gpsUtil) {
        this.gpsUtil = gpsUtil;
    }

    @Async
    public VisitedLocation getUserLocation(User user) {
        return gpsUtil.getUserLocation(user.getUserId());
    }

    public List<Attraction> getAttractions() {
        return gpsUtil.getAttractions();
    }

    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

    public List<Attraction> getAttractionsByDistance(Location referenceLocation) {
        List<Attraction> attractions = getAttractions();
        Collections.sort(attractions, new Comparator<Location>() {

            @Override
            public int compare(Location loc1, Location loc2) {
                double dist1 = getDistance(loc1, referenceLocation);
                double dist2 = getDistance(loc2, referenceLocation);
                return Double.compare(dist1, dist2);
            }

        });
        return attractions;
    }
}
