package tourguide.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import tourguide.models.User;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TripDealService {

        private final TripPricer tripPricer;
        private final CurrencyUnit currency = Monetary.getCurrency("USD");
        private static final String APIKEY = "test-server-api-key";

        public TripDealService(TripPricer tripPricer) {
                this.tripPricer = tripPricer;
        }

        public List<Provider> getTripDeals(User user) {
                int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
                List<Provider> providers = tripPricer.getPrice(APIKEY, user.getUserId(),
                                user.getUserPreferences().getNumberOfAdults(),
                                user.getUserPreferences().getNumberOfChildren(),
                                user.getUserPreferences().getTripDuration(),
                                cumulatativeRewardPoints);
                List<Provider> streamed = providers.stream().filter(
                                provider -> Money.of(provider.price, currency)
                                                .isGreaterThanOrEqualTo(user.getUserPreferences().getLowerPricePoint()))
                                .filter(
                                                provider -> Money.of(provider.price, currency)
                                                                .isLessThanOrEqualTo(user.getUserPreferences()
                                                                                .getHighPricePoint()))
                                .collect(Collectors.toList());
                user.setTripDeals(streamed);
                return streamed;
        }

}
