package tourguide.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.javamoney.moneta.Money;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourguide.dto.NearbyAttractionDto;
import tourguide.dto.UserPreferencesDto;
import tourguide.models.User;
import tourguide.models.UserPreferences;
import tourguide.repositories.UserRepository;
import tripPricer.Provider;

@Service
public class UserService {

    private final ExecutorService executorService = Executors.newFixedThreadPool(50);

    private final UserRepository repository;
    private final GpsService gpsService;
    private final RewardsService rewardsService;
    private final TripDealService tripDealService;

    public UserService(UserRepository repository, GpsService gpsService, RewardsService rewardsService,
            TripDealService tripDealService) {
        this.repository = repository;
        this.gpsService = gpsService;
        this.rewardsService = rewardsService;
        this.tripDealService = tripDealService;
    }

    public User getUser(String userName) {
        return repository.findByUsername(userName).orElseThrow(() -> new IllegalArgumentException("User not found"));
    }

    public List<User> getAllUsers() {
        return repository.findallUsers();
    }

    @Async
    public VisitedLocation trackUserLocation(User user) {
        VisitedLocation visitedLocation = gpsService.getUserLocation(user);
        user.addToVisitedLocations(visitedLocation);
        rewardsService.calculateRewards(user);
        return visitedLocation;
    }

    public List<VisitedLocation> trackUsersLocations(List<User> users) {
        List<VisitedLocation> locations = new ArrayList<>();
        List<CompletableFuture<Void>> futures = new ArrayList<>();

        for (User user : users) {
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                locations.add(trackUserLocation(user));
            }, executorService);
            futures.add(future);
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

        return locations;
    }

    public VisitedLocation getUserLocation(User user) {
        return !user.getVisitedLocations().isEmpty() ? user.getLastVisitedLocation() : trackUserLocation(user);
    }

    public Map<UUID, Location> getAllCurrentLocations() {
        Map<UUID, Location> result = new HashMap<>();
        List<User> users = repository.findallUsers();
        for (User user : users) {
            result.put(user.getUserId(), user.getLastVisitedLocation().location);
        }
        return result;
    }

    public List<NearbyAttractionDto> getNearByAttractions(User user, int numberOfAttractions) {

        VisitedLocation visitedLocation = gpsService.getUserLocation(user);
        List<NearbyAttractionDto> nearbyAttractions = new ArrayList<>();

        List<Attraction> attractions = gpsService.getAttractionsByDistance(visitedLocation.location);

        for (int i = 0; i < numberOfAttractions; i++) {
            NearbyAttractionDto nearbyAttractionDto = new NearbyAttractionDto();
            Attraction attraction = attractions.get(i);

            nearbyAttractionDto.setAttractionName(attraction.attractionName);
            nearbyAttractionDto.setAttractionLatitude(attraction.latitude);
            nearbyAttractionDto.setAttractionLongitude(attraction.longitude);
            nearbyAttractionDto.setUserLatitude(visitedLocation.location.latitude);
            nearbyAttractionDto.setUserLongitude(visitedLocation.location.longitude);
            nearbyAttractionDto.setDistance(gpsService.getDistance(attraction, visitedLocation.location));
            nearbyAttractionDto.setRewardPoints(rewardsService.getRewardPoints(attraction, user));

            nearbyAttractions.add(nearbyAttractionDto);
        }

        return nearbyAttractions;
    }

    public UserPreferences updateUserPreferences(UserPreferencesDto userPreferencesDto) {
        User user = getUser(userPreferencesDto.getUserName());
        UserPreferences userPreferences = user.getUserPreferences();
        if (userPreferencesDto.getAttractionProximity() != null) {
            userPreferences.setAttractionProximity(userPreferencesDto.getAttractionProximity());
        }
        if (userPreferencesDto.getCurrency() != null) {
            userPreferences.setCurrency(userPreferencesDto.getCurrency());
        }
        if (userPreferencesDto.getLowerPricePoint() != null) {
            Money money = Money.of(userPreferencesDto.getLowerPricePoint(), userPreferences.getCurrency());
            userPreferences.setLowerPricePoint(money);
        }
        if (userPreferencesDto.getHighPricePoint() != null) {
            Money money = Money.of(userPreferencesDto.getHighPricePoint(), userPreferences.getCurrency());
            userPreferences.setHighPricePoint(money);
        }
        if (userPreferencesDto.getTripDuration() != null) {
            userPreferences.setTripDuration(userPreferencesDto.getTripDuration());
        }
        if (userPreferencesDto.getTicketQuantity() != null) {
            userPreferences.setTicketQuantity(userPreferencesDto.getTicketQuantity());
        }
        if (userPreferencesDto.getNumberOfAdults() != null) {
            userPreferences.setNumberOfAdults(userPreferencesDto.getNumberOfAdults());
        }
        if (userPreferencesDto.getNumberOfChildren() != null) {
            userPreferences.setNumberOfChildren(userPreferencesDto.getNumberOfChildren());
        }

        user.setUserPreferences(userPreferences);
        User result = repository.updateUser(user).orElseThrow(() -> new IllegalArgumentException("User not found"));
        return result.getUserPreferences();

    }

    public List<Provider> getTripDeals(User user) {
        return tripDealService.getTripDeals(user);

    }
}
