package tourguide.dto;

import javax.money.CurrencyUnit;

public class UserPreferencesDto {

    private String userName;

    private Integer attractionProximity;
    private CurrencyUnit currency;
    private Double lowerPricePoint;
    private Double highPricePoint;
    private Integer tripDuration;
    private Integer ticketQuantity;
    private Integer numberOfAdults;
    private Integer numberOfChildren;

    public UserPreferencesDto() {
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAttractionProximity() {
        return this.attractionProximity;
    }

    public void setAttractionProximity(Integer attractionProximity) {
        this.attractionProximity = attractionProximity;
    }

    public CurrencyUnit getCurrency() {
        return this.currency;
    }

    public void setCurrency(CurrencyUnit currency) {
        this.currency = currency;
    }

    public Double getLowerPricePoint() {
        return this.lowerPricePoint;
    }

    public void setLowerPricePoint(Double lowerPricePoint) {
        this.lowerPricePoint = lowerPricePoint;
    }

    public Double getHighPricePoint() {
        return this.highPricePoint;
    }

    public void setHighPricePoint(Double highPricePoint) {
        this.highPricePoint = highPricePoint;
    }

    public Integer getTripDuration() {
        return this.tripDuration;
    }

    public void setTripDuration(Integer tripDuration) {
        this.tripDuration = tripDuration;
    }

    public Integer getTicketQuantity() {
        return this.ticketQuantity;
    }

    public void setTicketQuantity(Integer ticketQuantity) {
        this.ticketQuantity = ticketQuantity;
    }

    public Integer getNumberOfAdults() {
        return this.numberOfAdults;
    }

    public void setNumberOfAdults(Integer numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    public Integer getNumberOfChildren() {
        return this.numberOfChildren;
    }

    public void setNumberOfChildren(Integer numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

}
